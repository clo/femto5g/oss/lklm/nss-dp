/*
 * Copyright (c) 2022, Qualcomm Innovation Center, Inc. All rights reserved.
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <asm/cacheflush.h>
#include <linux/irq.h>
#include "edma.h"
#include "edma_regs.h"
#include "edma_debug.h"
#include "nss_dp_dev.h"
#include "edma_ppeds_priv.h"

#ifdef NSS_DP_PPEDS_SUPPORT
static char edma_ppeds_txcmpl_irq_name[EDMA_PPEDS_MAX_NODES][EDMA_IRQ_NAME_SIZE];
static char edma_ppeds_rxdesc_irq_name[EDMA_PPEDS_MAX_NODES][EDMA_IRQ_NAME_SIZE];
static char edma_ppeds_rxfill_irq_name[EDMA_PPEDS_MAX_NODES][EDMA_IRQ_NAME_SIZE];

/*
 * edma_ppeds_rx_fill_ring_alloc()
 *	API to allocate Rxfill ring for PPE-DS node
 */
static int edma_ppeds_rx_fill_ring_alloc(struct edma_rxfill_ring *rxfill_ring)
{
	/*
	 * Allocate RxFill ring descriptors
	 */
	rxfill_ring->desc = kmalloc((sizeof(struct edma_rxfill_desc) * rxfill_ring->count) +
				SMP_CACHE_BYTES,  GFP_KERNEL | __GFP_ZERO);
	if (!rxfill_ring->desc) {
		edma_err("Descriptor alloc for RXFILL ring %u failed\n",
							rxfill_ring->ring_id);
		return -ENOMEM;
	}

	rxfill_ring->dma = (dma_addr_t)virt_to_phys(rxfill_ring->desc);

	return 0;
}

/*
 * edma_ppeds_rx_fill_ring_free()
 *	API to free Rxfill ring for PPE-DS node
 */
static void edma_ppeds_rx_fill_ring_free(struct edma_rxfill_ring *rxfill_ring)
{
	/*
	 * Free RXFILL ring descriptors
	 */
	kfree(rxfill_ring->desc);
	rxfill_ring->desc = NULL;
	rxfill_ring->dma = (dma_addr_t)0;
}

/*
 * edma_ppeds_rx_secondary_alloc()
 *	API to allocate secondary Rx ring for PPE-DS node
 */
static int edma_ppeds_rx_secondary_alloc(struct edma_rxdesc_ring *rxdesc_ring)
{
	/*
	 * Allocate secondary RxDesc ring descriptors
	 */
	rxdesc_ring->sdesc = kmalloc((sizeof(struct edma_rxdesc_sec_desc) *  rxdesc_ring->count) +
			SMP_CACHE_BYTES,  GFP_KERNEL | __GFP_ZERO);
	if (!rxdesc_ring->sdesc) {
		edma_err("Descriptor alloc for secondary RX ring %u failed\n",
				rxdesc_ring->ring_id);
		return -1;
	}

	rxdesc_ring->sdma = (dma_addr_t)virt_to_phys(rxdesc_ring->sdesc);
	return 0;
}

/*
 * edma_ppeds_rx_secondary_free()
 *	API to free secondary Rx ring for PPE-DS node
 */
static void edma_ppeds_rx_secondary_free(struct edma_rxdesc_ring *rxdesc_ring)
{
	kfree(rxdesc_ring->sdesc);
	rxdesc_ring->sdesc = NULL;
	rxdesc_ring->sdma = (dma_addr_t)0;
}

/*
 * edma_ppeds_tx_cmpl_ring_alloc()
 *	API to allocate Tx complete ring for PPE-DS node
 */
static int edma_ppeds_tx_cmpl_ring_alloc(struct edma_txcmpl_ring *txcmpl_ring)
{
	txcmpl_ring->desc = kmalloc((sizeof(struct edma_txcmpl_desc) *  txcmpl_ring->count) +
				SMP_CACHE_BYTES,  GFP_KERNEL | __GFP_ZERO);
	if (!txcmpl_ring->desc) {
		edma_err("Descriptor alloc for TXCMPL ring %u failed\n",
				txcmpl_ring->id);
		return -ENOMEM;
	}

	txcmpl_ring->dma = (dma_addr_t)virt_to_phys(txcmpl_ring->desc);

	return 0;
}

/*
 * edma_ppeds_tx_cmpl_ring_free()
 *	API to free Tx complete ring for PPE-DS node
 */
static void edma_ppeds_tx_cmpl_ring_free(struct edma_txcmpl_ring *txcmpl_ring)
{
	kfree(txcmpl_ring->desc);
	txcmpl_ring->desc = NULL;
	txcmpl_ring->dma = (dma_addr_t)0;
}

/*
 * edma_ppeds_tx_secondary_alloc()
 *	API to allocate secondary Tx ring for PPE-DS node
 */
static int edma_ppeds_tx_secondary_alloc(struct edma_txdesc_ring *txdesc_ring)
{
	/*
	 * Allocate sencondary Tx ring descriptors
	 */
	txdesc_ring->sdesc = kmalloc((sizeof(struct edma_sec_txdesc) *  txdesc_ring->count) +
			SMP_CACHE_BYTES,  GFP_KERNEL | __GFP_ZERO);
	if (!txdesc_ring->sdesc) {
		edma_err("Descriptor alloc for secondary TX ring %u failed\n",
				txdesc_ring->id);
		return -1;
	}

	txdesc_ring->sdma = (dma_addr_t)virt_to_phys(txdesc_ring->sdesc);
	edma_debug("tx sec desc got allocated for Tx ring %d\n", txdesc_ring->id);
	return 0;
}

/*
 * edma_ppeds_tx_secondary_free()
 *	API to free secondary Tx ring for PPE-DS node
 */
static int edma_ppeds_tx_secondary_free(struct edma_txdesc_ring *txdesc_ring)
{
	kfree(txdesc_ring->sdesc);
	txdesc_ring->sdesc = NULL;
	txdesc_ring->sdma = (dma_addr_t)0;
	return 0;
}

/*
 * edma_ppeds_tx_complete()
 *	PPE-DS EDMA Tx complete processing API
 */
static uint32_t edma_ppeds_tx_complete(uint32_t work_to_do, struct edma_txcmpl_ring *txcmpl_ring)
{
	struct edma_ppeds *ppeds_node = container_of(txcmpl_ring, struct edma_ppeds, txcmpl_ring);
	edma_ppeds_handle_t *ppeds_handle = &ppeds_node->ppeds_handle;
	struct edma_txcmpl_desc *txcmpl;
	uint32_t cons_idx, prod_idx, data, avail, end_idx;
	uint16_t count;

	cons_idx = txcmpl_ring->cons_idx;

	/*
	 * Get TXCMPL ring producer index
	 */
	data = edma_reg_read(EDMA_REG_TXCMPL_PROD_IDX(txcmpl_ring->id));
	prod_idx = data & EDMA_TXCMPL_PROD_IDX_MASK;

	avail = EDMA_DESC_AVAIL_COUNT(prod_idx, cons_idx, txcmpl_ring->count);
	if (!avail) {
		return 0;
	}

	avail = min(avail, work_to_do);

	count = avail;

	end_idx = (cons_idx + avail) & (txcmpl_ring->count - 1);
	txcmpl = EDMA_TXCMPL_DESC(txcmpl_ring, cons_idx);

	if (end_idx > cons_idx) {
		dmac_inv_range_no_dsb((void *)txcmpl, txcmpl + avail);
	} else {
		dmac_inv_range_no_dsb(txcmpl_ring->desc, txcmpl_ring->desc + end_idx);
		dmac_inv_range_no_dsb((void *)txcmpl, txcmpl_ring->desc + txcmpl_ring->count);
	}

	dsb(st);

	while (likely(avail--)) {
		ppeds_handle->tx_cmpl_arr[count - avail - 1].cookie = EDMA_TXCMPL_OPAQUE_GET(txcmpl);

		cons_idx = ((cons_idx + 1) & (txcmpl_ring->count - 1));
		txcmpl = EDMA_TXCMPL_DESC(txcmpl_ring, cons_idx);
	}

	txcmpl_ring->cons_idx = cons_idx;
	edma_reg_write(EDMA_REG_TXCMPL_CONS_IDX(txcmpl_ring->id), cons_idx);

	ppeds_node->ops->tx_cmpl(ppeds_handle, count);
	return count;
}

/*
 * edma_ppeds_txcomp_napi_poll()
 *	PPE-DS EDMA TX NAPI handler
 */
static int edma_ppeds_txcomp_napi_poll(struct napi_struct *napi, int budget)
{
	struct edma_txcmpl_ring *txcmpl_ring = (struct edma_txcmpl_ring *)napi;
	struct edma_gbl_ctx *egc = &edma_gbl_ctx;
	uint32_t txcmpl_intr_status;
	int work_done = 0;
	uint32_t reg_data;

	do {
		work_done += edma_ppeds_tx_complete(budget - work_done, txcmpl_ring);
		if (work_done >= budget) {
			return work_done;
		}

		reg_data = edma_reg_read(EDMA_REG_TX_INT_STAT(txcmpl_ring->id));
		txcmpl_intr_status = reg_data & EDMA_TXCMPL_RING_INT_STATUS_MASK;
	} while (txcmpl_intr_status);

	/*
	 * No more packets to process. Finish NAPI processing.
	 */
	napi_complete(napi);

	/*
	 * Set TXCMPL ring interrupt mask
	 */
	edma_reg_write(EDMA_REG_TX_INT_MASK(txcmpl_ring->id),
			egc->txcmpl_intr_mask);

	return work_done;
}

/*
 * edma_rx_alloc_buffer()
 *	Alloc Rx buffers for RxFill ring
 */
static void edma_ppeds_rx_alloc_buffer(struct edma_rxfill_ring *rxfill_ring, int alloc_count, struct edma_ppeds_rx_fill_elem *rx_fill_arr,
		uint32_t headroom)
{
	struct edma_rxfill_desc *rxfill_desc;
	uint16_t prod_idx, start_idx;
	uint16_t num_alloc = 0;
	uint32_t rx_alloc_size = rxfill_ring->alloc_size;
	uint32_t ring_size_mask = rxfill_ring->count -1 ;

	/*
	 * Get RXFILL ring producer index
	 */
	prod_idx = rxfill_ring->prod_idx;
	start_idx = prod_idx;

	while (likely(alloc_count--)) {
		/*
		 * Get RXFILL descriptor
		 */
		rxfill_desc = EDMA_RXFILL_DESC(rxfill_ring, prod_idx);

		EDMA_RXFILL_BUFFER_ADDR_SET(rxfill_desc, rx_fill_arr[num_alloc].buff_addr);
		rxfill_desc->word2 = rx_fill_arr[num_alloc].opaque_lo;
		rxfill_desc->word3 = rx_fill_arr[num_alloc].opaque_hi;
		EDMA_RXFILL_PACKET_LEN_SET(rxfill_desc,
			cpu_to_le32((uint32_t)
			(rx_alloc_size - headroom)
			& EDMA_RXFILL_BUF_SIZE_MASK));

		prod_idx = (prod_idx + 1) & ring_size_mask;
		num_alloc++;
	}

	if (likely(num_alloc)) {
		uint16_t end_idx =
			(start_idx + num_alloc) & ring_size_mask;

		rxfill_desc = EDMA_RXFILL_DESC(rxfill_ring, start_idx);

		/*
		 * Write-back all the cached descriptors
		 * that are processed.
		 */
		if (end_idx > start_idx) {
			dmac_clean_range((void *)rxfill_desc,
					(void *)(rxfill_desc + num_alloc));
		} else {
			dmac_clean_range((void *)rxfill_ring->desc,
					(void *)(rxfill_ring->desc + end_idx));
			dmac_clean_range((void *)rxfill_desc,
					(void *)(rxfill_ring->desc +
							rxfill_ring->count));
		}

		edma_reg_write(EDMA_REG_RXFILL_PROD_IDX(rxfill_ring->ring_id),
								prod_idx);
		rxfill_ring->prod_idx = prod_idx;
	}
}

/*
 * edma_ppeds_rxfill_napi_poll()
 *	EDMA RXFill NAPI handler
 */
static int edma_ppeds_rxfill_napi_poll(struct napi_struct *napi, int budget)
{
	uint32_t cons_idx, work_to_do, status;
	uint32_t num_avail = 0;
	struct edma_rxfill_ring *rxfill_ring = (struct edma_rxfill_ring *)napi;
	struct edma_ppeds *ppeds_node = container_of(rxfill_ring, struct edma_ppeds, rxfill_ring);
	uint32_t alloc_size = rxfill_ring->alloc_size;
	uint32_t headroom = EDMA_RX_SKB_HEADROOM + NET_IP_ALIGN;

	do {
		cons_idx =
			edma_reg_read(EDMA_REG_RXFILL_CONS_IDX(rxfill_ring->ring_id)) &
			EDMA_RXFILL_CONS_IDX_MASK;
		work_to_do = (cons_idx - rxfill_ring->prod_idx + rxfill_ring->count - 1) & (rxfill_ring->count - 1);

		if (work_to_do > budget) {
			work_to_do = budget;
		}

		if (likely(work_to_do > 0)) {
			num_avail = ppeds_node->ops->rx_fill(&ppeds_node->ppeds_handle, work_to_do, alloc_size, headroom);
			edma_ppeds_rx_alloc_buffer(rxfill_ring, num_avail, ppeds_node->ppeds_handle.rx_fill_arr, headroom);
		}

		/*
		 * Return if budget has exhausted
		 */
		if (likely(num_avail >= budget)) {
			return num_avail;
		}

		/*
		 * Read on Clear
		 */
		status = EDMA_RXFILL_RING_INT_STATUS_MASK &
			edma_reg_read(EDMA_REG_RXFILL_INT_STAT(rxfill_ring->ring_id));
	} while (likely(status));

	napi_complete(napi);
	edma_reg_write(EDMA_REG_RXFILL_INT_MASK(rxfill_ring->ring_id),
			EDMA_RXFILL_INT_MASK);

	return 0;
}

/*
 * edma_ppeds_rx_napi_poll()
 *	EDMA RX NAPI handler
 */
static int edma_ppeds_rx_napi_poll(struct napi_struct *napi, int budget)
{
	struct edma_rxdesc_ring *rxdesc_ring = (struct edma_rxdesc_ring *)napi;
	struct edma_ppeds *ppeds_node = container_of(rxdesc_ring, struct edma_ppeds, rx_ring);
	uint16_t prod_idx;
	uint32_t status;

	/*
	 * Read EDMA Prod Idx.
	 */
	prod_idx =
		edma_reg_read(EDMA_REG_RXDESC_PROD_IDX(rxdesc_ring->ring_id)) &
		EDMA_RXDESC_PROD_IDX_MASK;

	/*
	 * Update Prod idx to DS interface
	 */
	ppeds_node->ops->rx(&ppeds_node->ppeds_handle, prod_idx);


	/*
	 * Read on Clear
	 */
	status = EDMA_RXDESC_RING_INT_STATUS_MASK &
		edma_reg_read(EDMA_REG_RXDESC_INT_STAT(rxdesc_ring->ring_id));

	napi_complete(napi);

	/*
	 * Set RXDESC ring interrupt mask
	 */
	edma_reg_write(EDMA_REG_RXDESC_INT_MASK(rxdesc_ring->ring_id),
			EDMA_RXDESC_INT_MASK_PKT_INT);

	return 0;
}

/*
 * edma_ppeds_set_rx_mapping()
 *	API for PPE-DS EDMA Rx ring mapping
 */
static void edma_ppeds_set_rx_mapping(uint32_t rxfill_ring_id, uint32_t rx_ring_id, uint32_t ppe_qid,
		uint32_t num_ppe_queues)
{
	uint32_t reg, data;
        uint8_t start_reg_index = ppe_qid/EDMA_QID2RID_NUM_PER_REG;
        uint8_t start_qnum = ppe_qid - (start_reg_index * EDMA_QID2RID_NUM_PER_REG);

	/*
	 * Setup RxFill to Rx mapping.
	 */
	if ((rx_ring_id >= 0) && (rx_ring_id <= 9)) {
		reg = EDMA_REG_RXDESC2FILL_MAP_0;
	} else if ((rx_ring_id >= 10) && (rx_ring_id <= 19)) {
		reg = EDMA_REG_RXDESC2FILL_MAP_1;
	} else {
		reg = EDMA_REG_RXDESC2FILL_MAP_2;
	}

	data = edma_reg_read(reg);
	data |= (rxfill_ring_id &
			EDMA_RXDESC2FILL_MAP_RXDESC_MASK) <<
		((rx_ring_id % 10) * 3);
	edma_reg_write(reg, data);

	/*
	 * Setup PPE Queue to Rx Ring mapping.
	 */
	while (num_ppe_queues > 0) {
		data = edma_reg_read(EDMA_QID2RID_TABLE_MEM(start_reg_index));
                switch (start_qnum) {
                case 0:
                        data &= (~EDMA_RX_RING_ID_QUEUE0_MASK);
                        data |= EDMA_RX_RING_ID_QUEUE0_SET(rx_ring_id);
                        num_ppe_queues--;
			if (num_ppe_queues == 0) {
				break;
			}
			/* fall through */
		case 1:
			data &= (~EDMA_RX_RING_ID_QUEUE1_MASK);
                        data |= EDMA_RX_RING_ID_QUEUE1_SET(rx_ring_id);
                        num_ppe_queues--;
                        if (num_ppe_queues == 0) {
                                break;
                        }
			/* fall through */
                case 2:
                        data &= (~EDMA_RX_RING_ID_QUEUE2_MASK);
                        data |= EDMA_RX_RING_ID_QUEUE2_SET(rx_ring_id);
                        num_ppe_queues--;
                        if (num_ppe_queues == 0) {
                                break;
                        }
			/* fall through */
                case 3:
                        data &= (~EDMA_RX_RING_ID_QUEUE3_MASK);
                        data |= EDMA_RX_RING_ID_QUEUE3_SET(rx_ring_id);
                        num_ppe_queues--;
                        if (num_ppe_queues == 0) {
                                break;
                        }
		}
		start_qnum = 0;
		edma_reg_write(EDMA_QID2RID_TABLE_MEM(start_reg_index), data);
		start_reg_index++;
	}

	edma_debug("PPE Queue %d mapped to EDMA ring %d", ppe_qid, rx_ring_id);
}

/*
 * edma_ppeds_set_tx_mapping()
 *	API for PPE-DS EDMA Tx ring mapping
 */
static void edma_ppeds_set_tx_mapping(uint32_t tx_ring_id, uint32_t txcmpl_ring_id)
{
	uint32_t reg, data;

	/*
	 * Setup TxDesc to TxComplete mapping.
	 */
	if ((tx_ring_id >= 0) && (tx_ring_id <= 5)) {
		reg = EDMA_REG_TXDESC2CMPL_MAP_0;
	} else if ((tx_ring_id >= 6) && (tx_ring_id <= 11)) {
		reg = EDMA_REG_TXDESC2CMPL_MAP_1;
	} else if ((tx_ring_id >= 12) && (tx_ring_id <= 17)) {
		reg = EDMA_REG_TXDESC2CMPL_MAP_2;
	} else if ((tx_ring_id >= 18) && (tx_ring_id <= 23)) {
		reg = EDMA_REG_TXDESC2CMPL_MAP_3;
	} else if ((tx_ring_id >= 24) && (tx_ring_id <= 29)) {
		reg = EDMA_REG_TXDESC2CMPL_MAP_4;
	} else {
		reg = EDMA_REG_TXDESC2CMPL_MAP_5;
	}

	data = edma_reg_read(reg);
	data |= (txcmpl_ring_id & EDMA_TXDESC2CMPL_MAP_TXDESC_MASK) << ((tx_ring_id % 6) * 5);
	edma_reg_write(reg, data);
}

/*
 * edma_ppeds_cfg_tx()
 *	API to configure PPE-DS EDMA Tx ring
 */
static void edma_ppeds_cfg_tx(struct edma_ppeds *ppeds_node)
{
	uint32_t tx_mod_timer;
	struct edma_txdesc_ring *txdesc_ring = &ppeds_node->tx_ring;
	struct edma_txcmpl_ring *txcmpl_ring = &ppeds_node->txcmpl_ring;

	/*
	 * Configure TXDESC ring
	 */
	edma_reg_write(EDMA_REG_TXDESC_BA(txdesc_ring->id),
			(uint32_t)(txdesc_ring->pdma &
				EDMA_RING_DMA_MASK));

	edma_reg_write(EDMA_REG_TXDESC_BA2(txdesc_ring->id),
			(uint32_t)(txdesc_ring->sdma &
				EDMA_RING_DMA_MASK));

	edma_reg_write(EDMA_REG_TXDESC_RING_SIZE(txdesc_ring->id),
			(uint32_t)(txdesc_ring->count &
				EDMA_TXDESC_RING_SIZE_MASK));

	edma_reg_write(EDMA_REG_TXDESC_PROD_IDX(txdesc_ring->id), EDMA_TX_INITIAL_PROD_IDX);

	/*
	 * Configure TxCmpl ring base address
	 */
	edma_reg_write(EDMA_REG_TXCMPL_BA(txcmpl_ring->id),
			(uint32_t)(txcmpl_ring->dma & EDMA_RING_DMA_MASK));
	edma_reg_write(EDMA_REG_TXCMPL_RING_SIZE(txcmpl_ring->id),
			(uint32_t)(txcmpl_ring->count
			& EDMA_TXDESC_RING_SIZE_MASK));

	/*
	 * Set TxCmpl ret mode to opaque
	 */
	edma_reg_write(EDMA_REG_TXCMPL_CTRL(txcmpl_ring->id),
			EDMA_TXCMPL_RETMODE_OPAQUE);

	/*
	 * Configure the default timer mitigation value
	 */
	tx_mod_timer = (EDMA_TX_MOD_TIMER & EDMA_TX_MOD_TIMER_INIT_MASK)
			<< EDMA_TX_MOD_TIMER_INIT_SHIFT;
	edma_reg_write(EDMA_REG_TX_MOD_TIMER(txcmpl_ring->id),
				tx_mod_timer);

	edma_reg_write(EDMA_REG_TX_INT_CTRL(txcmpl_ring->id), EDMA_TX_NE_INT_EN);
}

/*
 * edma_ppeds_cfg_rx()
 *	API to configure PPE-DS EDMA Rx ring
 */
static void edma_ppeds_cfg_rx(struct edma_ppeds *ppeds_node)
{
	struct edma_rxfill_ring *rxfill_ring = &ppeds_node->rxfill_ring;
	struct edma_rxdesc_ring *rxdesc_ring = &ppeds_node->rx_ring;
	uint32_t ring_sz;
	uint32_t data;

	edma_reg_write(EDMA_REG_RXFILL_BA(rxfill_ring->ring_id),
			(uint32_t)(rxfill_ring->dma & EDMA_RING_DMA_MASK));

	ring_sz = rxfill_ring->count & EDMA_RXFILL_RING_SIZE_MASK;
	edma_reg_write(EDMA_REG_RXFILL_RING_SIZE(rxfill_ring->ring_id), ring_sz);

	edma_reg_write(EDMA_REG_RXDESC_BA(rxdesc_ring->ring_id),
			(uint32_t)(rxdesc_ring->pdma & EDMA_RXDESC_BA_MASK));

	edma_reg_write(EDMA_REG_RXDESC_PREHEADER_BA(rxdesc_ring->ring_id),
			(uint32_t)(rxdesc_ring->sdma & EDMA_RXDESC_PREHEADER_BA_MASK));

	data = rxdesc_ring->count & EDMA_RXDESC_RING_SIZE_MASK;
	data |= (EDMA_RXDESC_PL_DEFAULT_VALUE & EDMA_RXDESC_PL_OFFSET_MASK)
		 << EDMA_RXDESC_PL_OFFSET_SHIFT;
	edma_reg_write(EDMA_REG_RXDESC_RING_SIZE(rxdesc_ring->ring_id), data);

	/*
	 * Configure the default timer mitigation value
	 */
	data = (EDMA_RX_MOD_TIMER_INIT & EDMA_RX_MOD_TIMER_INIT_MASK)
			<< EDMA_RX_MOD_TIMER_INIT_SHIFT;
	edma_reg_write(EDMA_REG_RX_MOD_TIMER(rxdesc_ring->ring_id), data);

	/*
	 * Enable ring. Set ret mode to 'opaque'.
	 */
	edma_reg_write(EDMA_REG_RX_INT_CTRL(rxdesc_ring->ring_id), EDMA_RX_NE_INT_EN);
}

/*
 * edma_ppeds_inst_register()
 *	PPE-DS EDMA instance registration API
 */
bool edma_ppeds_inst_register(edma_ppeds_handle_t *ppeds_handle)
{
	int ret;
	struct edma_ppeds *ppeds_node = container_of(ppeds_handle, struct edma_ppeds, ppeds_handle);
	uint32_t rx_ring_size = ppeds_handle->ppe2tcl_num_desc;
	uint32_t tx_ring_size = ppeds_handle->reo2ppe_num_desc;

	ppeds_node->rxfill_ring.count = rx_ring_size;
	ppeds_node->rxfill_ring.alloc_size  = NSS_DP_RX_BUFFER_SIZE;
	ppeds_node->rx_ring.count = rx_ring_size;
	ppeds_node->rx_ring.pdma = (dma_addr_t)ppeds_handle->ppe2tcl_ba;


	ret = edma_ppeds_rx_fill_ring_alloc(&ppeds_node->rxfill_ring);
	if (ret != 0) {
		return false;
	}

	ppeds_node->txcmpl_ring.count = tx_ring_size;
	ppeds_node->tx_ring.count = tx_ring_size;
	ppeds_node->tx_ring.pdma = (dma_addr_t)ppeds_handle->reo2ppe_ba;
	ppeds_node->tx_ring.pdesc = phys_to_virt(ppeds_node->tx_ring.pdma);

	memset(ppeds_node->tx_ring.pdesc, 0, 32 * tx_ring_size);

	ret = edma_ppeds_tx_cmpl_ring_alloc(&ppeds_node->txcmpl_ring);
	if (ret != 0) {
		edma_ppeds_rx_fill_ring_free(&ppeds_node->rxfill_ring);
		goto rx_fill_setup_failed;
	}

	ppeds_handle->rx_fill_arr =
		(struct edma_ppeds_rx_fill_elem *)kzalloc(sizeof(struct edma_ppeds_rx_fill_elem) * rx_ring_size, GFP_KERNEL);
	if (!ppeds_handle->rx_fill_arr) {
		goto rx_fill_arr_alloc_failed;
		return false;
	}

	ppeds_handle->tx_cmpl_arr=
		(struct edma_ppeds_tx_cmpl_elem*)kzalloc(sizeof(struct edma_ppeds_tx_cmpl_elem) * tx_ring_size, GFP_KERNEL);
	if (!ppeds_handle->tx_cmpl_arr) {
		goto tx_cmpl_arr_alloc_failed;
		return false;
	}

	/*
	 * Setup dummy netdev for all the NAPIs associated with this node
	 */
	init_dummy_netdev(&ppeds_node->napi_ndev);

	/*
	 * Setup TxComp IRQ and NAPI
	 */
	irq_set_status_flags(ppeds_node->txcmpl_intr, IRQ_DISABLE_UNLAZY);
	snprintf(edma_ppeds_txcmpl_irq_name[ppeds_node->db_idx], 32,
			 "edma_ppeds_txcmpl_%d", ppeds_node->db_idx);
	ret = request_irq(ppeds_node->txcmpl_intr,
			edma_tx_handle_irq, IRQF_SHARED,
			edma_ppeds_txcmpl_irq_name[ppeds_node->db_idx],
			(void *)&ppeds_node->txcmpl_ring);
	if (ret) {
		edma_err("PPEDS TXCMPL ring IRQ:%d request failed for node %d\n",
				ppeds_node->txcmpl_intr, ppeds_node->db_idx);
		goto txcomp_irq_fail;
	}

	netif_napi_add(&ppeds_node->napi_ndev, &ppeds_node->txcmpl_ring.napi,
			edma_ppeds_txcomp_napi_poll, ppeds_handle->eth_txcomp_budget);

	/*
	 * Setup RxDesc IRQ and NAPI
	 */
	irq_set_status_flags(ppeds_node->rxdesc_intr, IRQ_DISABLE_UNLAZY);
	snprintf(edma_ppeds_rxdesc_irq_name[ppeds_node->db_idx], 32,
			 "edma_ppeds_rxdesc_%d", ppeds_node->db_idx);
	ret = request_irq(ppeds_node->rxdesc_intr,
			edma_rx_handle_irq, IRQF_SHARED,
			edma_ppeds_rxdesc_irq_name[ppeds_node->db_idx],
			(void *)&ppeds_node->rx_ring);
	if (ret) {
		edma_err("PPEDS RXDESC ring IRQ:%d request failed for node %d\n",
				ppeds_node->rxdesc_intr, ppeds_node->db_idx);
		goto rxdesc_irq_fail;

	}

	netif_napi_add(&ppeds_node->napi_ndev, &ppeds_node->rx_ring.napi,
			edma_ppeds_rx_napi_poll, EDMA_PPEDS_RX_WEIGHT);

	/*
	 * Setup RxFill IRQ and NAPI
	 */
	irq_set_status_flags(ppeds_node->rxfill_intr, IRQ_DISABLE_UNLAZY);
	snprintf(edma_ppeds_rxfill_irq_name[ppeds_node->db_idx], 32,
			 "edma_ppeds_rxfill_%d", ppeds_node->db_idx);
	ret = request_irq(ppeds_node->rxfill_intr,
			edma_rxfill_handle_irq, IRQF_SHARED,
			edma_ppeds_rxfill_irq_name[ppeds_node->db_idx],
			(void *)&ppeds_node->rxfill_ring);
	if (ret) {
		edma_err("PPEDS RXFILL ring IRQ:%d request failed for node %d\n",
				ppeds_node->rxfill_intr, ppeds_node->db_idx);
		goto rxfill_irq_fail;

	}

	netif_napi_add(&ppeds_node->napi_ndev, &ppeds_node->rxfill_ring.napi,
			edma_ppeds_rxfill_napi_poll, EDMA_PPEDS_RXFILL_WEIGHT);

	ret = edma_ppeds_rx_secondary_alloc(&ppeds_node->rx_ring);
	if (ret) {
		edma_err("Failed to setup secondary EDMA Rx Desc Ring\n");
		goto rx_sec_setup_failed;
	}

	ret = edma_ppeds_tx_secondary_alloc(&ppeds_node->tx_ring);
	if (ret) {
		edma_err("Failed to setup secondary EDMA Tx Desc Ring\n");
		goto tx_sec_setup_failed;
	}

	/*
	 * Setup Tx and Rx mappings
	 */
	edma_ppeds_set_tx_mapping(ppeds_node->tx_ring.id, ppeds_node->txcmpl_ring.id);
	edma_ppeds_set_rx_mapping(ppeds_node->rxfill_ring.ring_id, ppeds_node->rx_ring.ring_id, ppeds_node->ppe_qid,
			ppeds_node->ppe_num_queues);

	edma_ppeds_cfg_tx(ppeds_node);
	edma_ppeds_cfg_rx(ppeds_node);

	edma_debug("EDMA PPEDS registeration succesful\n");

	return true;

tx_sec_setup_failed:
	edma_ppeds_rx_secondary_free(&ppeds_node->rx_ring);
rx_sec_setup_failed:
	irq_clear_status_flags(ppeds_node->rxfill_intr, IRQ_DISABLE_UNLAZY);
	synchronize_irq(ppeds_node->rxfill_intr);
	free_irq(ppeds_node->rxfill_intr,
			(void *)&ppeds_node->rxfill_ring);
	netif_napi_del(&ppeds_node->rxfill_ring.napi);
rxfill_irq_fail:
	irq_clear_status_flags(ppeds_node->rxdesc_intr, IRQ_DISABLE_UNLAZY);
	synchronize_irq(ppeds_node->rxdesc_intr);
	free_irq(ppeds_node->rxdesc_intr,
			(void *)&ppeds_node->rx_ring);
	netif_napi_del(&ppeds_node->rx_ring.napi);
rxdesc_irq_fail:
	irq_clear_status_flags(ppeds_node->txcmpl_intr, IRQ_DISABLE_UNLAZY);
	synchronize_irq(ppeds_node->txcmpl_intr);
	free_irq(ppeds_node->txcmpl_intr,
			(void *)&ppeds_node->txcmpl_ring);
	netif_napi_del(&ppeds_node->txcmpl_ring.napi);

txcomp_irq_fail:
	kfree(ppeds_handle->tx_cmpl_arr);
	ppeds_handle->tx_cmpl_arr= NULL;
tx_cmpl_arr_alloc_failed:
	kfree(ppeds_handle->rx_fill_arr);
	ppeds_handle->rx_fill_arr = NULL;
rx_fill_arr_alloc_failed:
	edma_ppeds_rx_fill_ring_free(&ppeds_node->rxfill_ring);
rx_fill_setup_failed:
	edma_ppeds_tx_cmpl_ring_free(&ppeds_node->txcmpl_ring);

	return false;

}
EXPORT_SYMBOL(edma_ppeds_inst_register);

/*
 * edma_ppeds_inst_refill()
 *	API to fill PPE-DS EDMA RxFill ring
 */
void edma_ppeds_inst_refill(edma_ppeds_handle_t *ppeds_handle, int count)
{
	uint32_t num_avail;
	uint32_t headroom = EDMA_RX_SKB_HEADROOM + NET_IP_ALIGN;
	struct edma_ppeds *ppeds_node = container_of(ppeds_handle, struct edma_ppeds, ppeds_handle);
	struct edma_rxfill_ring *rxfill_ring = &ppeds_node->rxfill_ring;

	num_avail = ppeds_node->ops->rx_fill(&ppeds_node->ppeds_handle, count, rxfill_ring->alloc_size, headroom);

	if(count != num_avail) {
		edma_warn("Got %d less than what is asked for %d\n", num_avail, count);
	}

	edma_ppeds_rx_alloc_buffer(rxfill_ring, num_avail,
		       	ppeds_node->ppeds_handle.rx_fill_arr, headroom);

}
EXPORT_SYMBOL(edma_ppeds_inst_refill);

/*
 * edma_ppeds_drain_rxfill_ring()
 *	PPE-DS EDMA API to drain the Rxfill ring
 */
void edma_ppeds_drain_rxfill_ring(edma_ppeds_handle_t *ppeds_handle)
{
	uint16_t cons_idx, curr_idx;
	uint32_t reg_data;
	struct edma_ppeds *ppeds_node = container_of(ppeds_handle, struct edma_ppeds, ppeds_handle);
	struct edma_rxfill_ring *rxfill_ring = &ppeds_node->rxfill_ring;

	/*
	 * Get RXFILL ring producer index
	 */
	curr_idx = rxfill_ring->prod_idx & EDMA_RXFILL_PROD_IDX_MASK;

	/*
	 * Get RXFILL ring consumer index
	 */
	reg_data = edma_reg_read(EDMA_REG_RXFILL_CONS_IDX(rxfill_ring->ring_id));
	cons_idx = reg_data & EDMA_RXFILL_CONS_IDX_MASK;

	while (curr_idx != cons_idx) {
		struct edma_rxfill_desc *rxfill_desc;
		uint64_t rx_opaque;

		/*
		 * Get RXFILL descriptor
		 */
		rxfill_desc = EDMA_RXFILL_DESC(rxfill_ring, cons_idx);

		cons_idx = (cons_idx + 1) & EDMA_RX_RING_SIZE_MASK;

		/*
		 * Release opaque to the DS interface
		 */
		rx_opaque = (uint64_t)EDMA_RXFILL_OPAQUE_GET(rxfill_desc);
		if (unlikely(!rx_opaque)) {
			edma_warn("Empty rx_opaque reference at index:%d\n",
					cons_idx);
			continue;
		}

		ppeds_node->ops->rx_release(&ppeds_node->ppeds_handle, rx_opaque);
	}
}
EXPORT_SYMBOL(edma_ppeds_drain_rxfill_ring);

/*
 * edma_ppeds_drain_tx_cmpl_ring()
 *	PPE-DS EDMA API to drain the Tx complete ring
 */
void edma_ppeds_drain_tx_cmpl_ring(edma_ppeds_handle_t *ppeds_handle)
{
	struct edma_ppeds *ppeds_node = container_of(ppeds_handle, struct edma_ppeds, ppeds_handle);
	struct edma_txcmpl_ring *txcmpl_ring = &ppeds_node->txcmpl_ring;

	edma_ppeds_tx_complete(txcmpl_ring->count - 1, txcmpl_ring);
}
EXPORT_SYMBOL(edma_ppeds_drain_tx_cmpl_ring);

/*
 * edma_ppeds_get_ppe_queues()
 *	Get the associated PPE queues with the given instance
 */
bool edma_ppeds_get_ppe_queues(edma_ppeds_handle_t *ppeds_handle, uint32_t *ppe_queue_start, uint32_t *num)
{
	struct edma_ppeds *ppeds_node = container_of(ppeds_handle, struct edma_ppeds, ppeds_handle);

	*ppe_queue_start = ppeds_node->ppe_qid;
	*num = ppeds_node->ppe_num_queues;

	return true;
}
EXPORT_SYMBOL(edma_ppeds_get_ppe_queues);

/*
 * edma_ppeds_set_tx_prod_idx()
 *	Set EDMA RX producer idx
 */
void edma_ppeds_set_tx_prod_idx(edma_ppeds_handle_t *ppeds_handle, uint16_t tx_prod_idx)
{
	struct edma_ppeds *ppeds_node = container_of(ppeds_handle, struct edma_ppeds, ppeds_handle);

	edma_reg_write(EDMA_REG_TXDESC_PROD_IDX(ppeds_node->tx_ring.id), tx_prod_idx);

}
EXPORT_SYMBOL(edma_ppeds_set_tx_prod_idx);

/*
 * edma_ppeds_set_rx_cons_idx()
 *	Set EDMA RX consumer idx
 */
void edma_ppeds_set_rx_cons_idx(edma_ppeds_handle_t *ppeds_handle, uint16_t rx_cons_idx)
{
	struct edma_ppeds *ppeds_node = container_of(ppeds_handle, struct edma_ppeds, ppeds_handle);

	edma_reg_write(EDMA_REG_RXDESC_CONS_IDX(ppeds_node->rx_ring.ring_id), rx_cons_idx);

}
EXPORT_SYMBOL(edma_ppeds_set_rx_cons_idx);

/*
 * edma_ppeds_get_rx_prod_idx()
 *	Get EDMA RX producer idx
 */
uint16_t edma_ppeds_get_rx_prod_idx(edma_ppeds_handle_t *ppeds_handle)
{
	uint32_t data;
	uint16_t prod_idx;
	struct edma_ppeds *ppeds_node = container_of(ppeds_handle, struct edma_ppeds, ppeds_handle);

	data = edma_reg_read(EDMA_REG_RXDESC_PROD_IDX(ppeds_node->rx_ring.ring_id));
	prod_idx = data & EDMA_RXDESC_PROD_IDX_MASK;

	return prod_idx;
}
EXPORT_SYMBOL(edma_ppeds_get_rx_prod_idx);

/*
 * edma_ppeds_get_tx_cons_idx()
 *	Get EDMA TX consumer idx
 */
uint16_t edma_ppeds_get_tx_cons_idx(edma_ppeds_handle_t *ppeds_handle)
{
	uint32_t data;
	uint16_t cons_idx;
	struct edma_ppeds *ppeds_node = container_of(ppeds_handle, struct edma_ppeds, ppeds_handle);

	data = edma_reg_read(EDMA_REG_TXDESC_CONS_IDX(ppeds_node->tx_ring.id));
	cons_idx = data & EDMA_TXDESC_CONS_IDX_MASK;

	return cons_idx;
}
EXPORT_SYMBOL(edma_ppeds_get_tx_cons_idx);

/*
 * edma_ppeds_inst_start()
 *	PPE-DS EDMA instance start API
 */
int edma_ppeds_inst_start(edma_ppeds_handle_t *ppeds_handle, uint8_t intr_enable)
{
	uint32_t data;
	struct edma_ppeds *ppeds_node = container_of(ppeds_handle, struct edma_ppeds, ppeds_handle);

	/*
	 * Configure RxFill Low threshold value and
	 * enable RXFILL Low threshold interrupt along with the
	 * associated NAPI
	 */
	edma_debug("Setting low threshold to %d\n", ppeds_handle->eth_rxfill_low_thr);

	edma_reg_write(EDMA_REG_RXFILL_UGT_THRE(ppeds_node->rxfill_ring.ring_id),
			EDMA_RXFILL_LOW_THRE_MASK & ppeds_handle->eth_rxfill_low_thr);
	edma_reg_write(EDMA_REG_RXFILL_INT_MASK(ppeds_node->rxfill_ring.ring_id),
			EDMA_RXFILL_INT_MASK);
	napi_enable(&ppeds_node->rxfill_ring.napi);

	/*
	 * Enable TxComp interrupt along with the associated NAPI
	 */
	edma_reg_write(EDMA_REG_TX_INT_MASK(ppeds_node->txcmpl_ring.id),
			EDMA_TX_INT_MASK_PKT_INT);
	napi_enable(&ppeds_node->txcmpl_ring.napi);

	/*
	 * Enable RxDesc Ring.
	 */
	data = edma_reg_read(EDMA_REG_RXDESC_CTRL(ppeds_node->rx_ring.ring_id));
	data |= EDMA_RXDESC_RX_EN;
	edma_reg_write(EDMA_REG_RXDESC_CTRL(ppeds_node->rx_ring.ring_id), data);

	/*
	 * Enable RxFill Ring.
	 */
	data = edma_reg_read(EDMA_REG_RXFILL_RING_EN(ppeds_node->rxfill_ring.ring_id));
	data |= EDMA_RXFILL_RING_EN;
	edma_reg_write(EDMA_REG_RXFILL_RING_EN(ppeds_node->rxfill_ring.ring_id), data);

	/*
	 * Enable Tx Ring.
	 */
	data = edma_reg_read(EDMA_REG_TXDESC_CTRL(ppeds_node->tx_ring.id));
	data |= EDMA_TXDESC_TX_ENABLE;
	edma_reg_write(EDMA_REG_TXDESC_CTRL(ppeds_node->tx_ring.id), data);

	return 0;
}
EXPORT_SYMBOL(edma_ppeds_inst_start);

/*
 * edma_ppeds_inst_stop()
 *	PPE-DS EDMA instance stop API
 */
void edma_ppeds_inst_stop(edma_ppeds_handle_t *ppeds_handle, uint8_t intr_enable)
{
	uint32_t data;
	struct edma_ppeds *ppeds_node = container_of(ppeds_handle, struct edma_ppeds, ppeds_handle);

	/*
	 * Disable Rx, TxComp and RxFill rings.
	 */
	data = edma_reg_read(EDMA_REG_RXDESC_CTRL(ppeds_node->rx_ring.ring_id));
	data &= ~EDMA_RXDESC_RX_EN;
	edma_reg_write(EDMA_REG_RXDESC_CTRL(ppeds_node->rx_ring.ring_id), data);

	data = edma_reg_read(EDMA_REG_RXFILL_RING_EN(ppeds_node->rxfill_ring.ring_id));
	data &= ~EDMA_RXFILL_RING_EN;
	edma_reg_write(EDMA_REG_RXFILL_RING_EN(ppeds_node->rxfill_ring.ring_id), data);

	data = edma_reg_read(EDMA_REG_TXDESC_CTRL(ppeds_node->tx_ring.id));
	data &= ~EDMA_TXDESC_TX_ENABLE;
	edma_reg_write(EDMA_REG_TXDESC_CTRL(ppeds_node->tx_ring.id), data);

	/*
	 * Disable Rxfill interrupt and NAPI
	 */
	edma_reg_write(EDMA_REG_RXFILL_INT_MASK(ppeds_node->rxfill_ring.ring_id),
			EDMA_MASK_INT_CLEAR);
	synchronize_irq(ppeds_node->rxfill_intr);
	napi_disable(&ppeds_node->rxfill_ring.napi);

	/*
	 * Disable Tx complete interrupt and NAPI
	 */
	edma_reg_write(EDMA_REG_TX_INT_MASK(ppeds_node->tx_ring.id),
			EDMA_MASK_INT_CLEAR);
	synchronize_irq(ppeds_node->txcmpl_intr);
	napi_disable(&ppeds_node->txcmpl_ring.napi);
}
EXPORT_SYMBOL(edma_ppeds_inst_stop);

/*
 * edma_ppeds_inst_del()
 *	PPE-DS EDMA instance delete API
 */
void edma_ppeds_inst_del(edma_ppeds_handle_t *ppeds_handle)
{
	struct edma_ppeds_drv *drv = &edma_gbl_ctx.ppeds_drv;
	struct edma_ppeds *ppeds_node = container_of(ppeds_handle, struct edma_ppeds, ppeds_handle);

	kfree(ppeds_handle->rx_fill_arr);
	ppeds_handle->rx_fill_arr = NULL;

	irq_clear_status_flags(ppeds_node->rxdesc_intr, IRQ_DISABLE_UNLAZY);
	free_irq(ppeds_node->rxdesc_intr,
			(void *)&ppeds_node->rx_ring);
	netif_napi_del(&ppeds_node->rx_ring.napi);

	irq_clear_status_flags(ppeds_node->txcmpl_intr, IRQ_DISABLE_UNLAZY);
	free_irq(ppeds_node->txcmpl_intr,
			(void *)&ppeds_node->txcmpl_ring);
	netif_napi_del(&ppeds_node->txcmpl_ring.napi);

	irq_clear_status_flags(ppeds_node->rxfill_intr, IRQ_DISABLE_UNLAZY);
	free_irq(ppeds_node->rxfill_intr,
			(void *)&ppeds_node->rxfill_ring);
	netif_napi_del(&ppeds_node->rxfill_ring.napi);

	edma_ppeds_rx_fill_ring_free(&ppeds_node->rxfill_ring);
	edma_ppeds_rx_secondary_free(&ppeds_node->rx_ring);
	edma_ppeds_tx_secondary_free(&ppeds_node->tx_ring);
	edma_ppeds_tx_cmpl_ring_free(&ppeds_node->txcmpl_ring);

	/*
	 * Remove from DB
	 */
	write_lock_bh(&drv->lock);
	drv->ppeds_db[ppeds_node->db_idx] = NULL;
	drv->node_state[ppeds_node->db_idx] = EDMA_PPEDS_NODE_STATE_AVAIL;
	write_unlock_bh(&drv->lock);

	kfree(ppeds_node);
}
EXPORT_SYMBOL(edma_ppeds_inst_del);

/*
 * edma_ppeds_inst_alloc()
 *	PPE-DS EDMA instance allocation API
 */
edma_ppeds_handle_t *edma_ppeds_inst_alloc(const struct edma_ppeds_ops *ops, size_t priv_size)
{
	struct edma_ppeds *ppeds_node;
	uint32_t i;
	struct edma_ppeds_drv *drv = &edma_gbl_ctx.ppeds_drv;
	int size = priv_size + sizeof(struct edma_ppeds);

	edma_debug("ppeds node size %lu total size %d\n", sizeof(struct edma_ppeds),  size);
	ppeds_node = (struct edma_ppeds *)kzalloc(size, GFP_KERNEL);
	if (!ppeds_node) {
		edma_err("Cannot allocate memory for ppeds node\n");
		return NULL;
	}

	ppeds_node->ops = ops;

	/*
	 * Add to Database
	 */
	write_lock_bh(&drv->lock);
	for (i = 0; i < drv->num_nodes; i++) {
		if (drv->node_state[i] == EDMA_PPEDS_NODE_STATE_AVAIL) {
			break;
		}
	}

	if (i == drv->num_nodes) {
		write_unlock_bh(&drv->lock);
		edma_err("Cannot get a free edma PPE-DS resource set\n");
		kfree(ppeds_node);
		return NULL;
	}

	drv->node_state[i] = EDMA_PPEDS_NODE_STATE_INUSE;
	drv->ppeds_db[i] = ppeds_node;
	ppeds_node->rxfill_ring.ring_id = drv->node_map[i][EDMA_PPEDS_ENTRY_RXFILL_IDX];
	ppeds_node->txcmpl_ring.id = drv->node_map[i][EDMA_PPEDS_ENTRY_TXCMPL_IDX];
	ppeds_node->rx_ring.ring_id = drv->node_map[i][EDMA_PPEDS_ENTRY_RX_IDX];
	ppeds_node->tx_ring.id = drv->node_map[i][EDMA_PPEDS_ENTRY_TX_IDX];
	ppeds_node->ppe_qid  = drv->node_map[i][EDMA_PPEDS_ENTRY_QID_START_IDX];
	ppeds_node->ppe_num_queues  = drv->node_map[i][EDMA_PPEDS_ENTRY_QID_COUNT_IDX];
	ppeds_node->txcmpl_intr  = drv->irq_map[i][EDMA_PPEDS_TXCOMP_IRQ_IDX];
	ppeds_node->rxfill_intr  = drv->irq_map[i][EDMA_PPEDS_RXFILL_IRQ_IDX];
	ppeds_node->rxdesc_intr  = drv->irq_map[i][EDMA_PPEDS_RXDESC_IRQ_IDX];
	ppeds_node->db_idx  = i;
	edma_debug("PPE-DS node(%d): Rxfill ring: %d, Tx complete ring: %d,"
			" Rx ring: %d, Tx ring: %d, Queue start id: %d,"
			" Queue count: %d, Tx complete interrupt: %d,"
			" Rxfill interrupt: %d, Rx interrupt: %d\n", i,
			ppeds_node->rxfill_ring.ring_id, ppeds_node->txcmpl_ring.id,
			ppeds_node->rx_ring.ring_id, ppeds_node->tx_ring.id,
			ppeds_node->ppe_qid, ppeds_node->ppe_num_queues,
			ppeds_node->txcmpl_intr, ppeds_node->rxfill_intr,
			ppeds_node->rxdesc_intr);
	write_unlock_bh(&drv->lock);

	return &ppeds_node->ppeds_handle;
}
EXPORT_SYMBOL(edma_ppeds_inst_alloc);

#else
/*
 * edma_ppeds_inst_register()
 *	PPE-DS EDMA instance registration API
 */
bool edma_ppeds_inst_register(edma_ppeds_handle_t *ppeds_handle)
{
	return true;
}
EXPORT_SYMBOL(edma_ppeds_inst_register);

/*
 * edma_ppeds_inst_refill()
 *	API to fill PPE-DS EDMA RxFill ring
 */
void edma_ppeds_inst_refill(edma_ppeds_handle_t *ppeds_handle, int count)
{
	return;
}
EXPORT_SYMBOL(edma_ppeds_inst_refill);

/*
 * edma_ppeds_drain_rxfill_ring()
 *	PPE-DS EDMA API to drain the Rxfill ring
 */
void edma_ppeds_drain_rxfill_ring(edma_ppeds_handle_t *ppeds_handle)
{
	return;
}
EXPORT_SYMBOL(edma_ppeds_drain_rxfill_ring);

/*
 * edma_ppeds_drain_tx_cmpl_ring()
 *	PPE-DS EDMA API to drain the Tx complete ring
 */
void edma_ppeds_drain_tx_cmpl_ring(edma_ppeds_handle_t *ppeds_handle)
{
	return;
}
EXPORT_SYMBOL(edma_ppeds_drain_tx_cmpl_ring);

/*
 * edma_ppeds_get_ppe_queues()
 *	Get the associated PPE queues with the given instance
 */
bool edma_ppeds_get_ppe_queues(edma_ppeds_handle_t *ppeds_handle, uint32_t *ppe_queue_start, uint32_t *num)
{
	return true;
}
EXPORT_SYMBOL(edma_ppeds_get_ppe_queues);

/*
 * edma_ppeds_set_tx_prod_idx()
 *	Set EDMA RX producer idx
 */
void edma_ppeds_set_tx_prod_idx(edma_ppeds_handle_t *ppeds_handle, uint16_t tx_prod_idx)
{
	return;
}
EXPORT_SYMBOL(edma_ppeds_set_tx_prod_idx);

/*
 * edma_ppeds_set_rx_cons_idx()
 *	Set EDMA RX consumer idx
 */
void edma_ppeds_set_rx_cons_idx(edma_ppeds_handle_t *ppeds_handle, uint16_t rx_cons_idx)
{
	return;
}
EXPORT_SYMBOL(edma_ppeds_set_rx_cons_idx);

/*
 * edma_ppeds_get_rx_prod_idx()
 *	Get EDMA RX producer idx
 */
uint16_t edma_ppeds_get_rx_prod_idx(edma_ppeds_handle_t *ppeds_handle)
{
	return 0;
}
EXPORT_SYMBOL(edma_ppeds_get_rx_prod_idx);

/*
 * edma_ppeds_get_tx_cons_idx()
 *	Get EDMA TX consumer idx
 */
uint16_t edma_ppeds_get_tx_cons_idx(edma_ppeds_handle_t *ppeds_handle)
{
	return 0;
}
EXPORT_SYMBOL(edma_ppeds_get_tx_cons_idx);

/*
 * edma_ppeds_inst_start()
 *	PPE-DS EDMA instance start API
 */
int edma_ppeds_inst_start(edma_ppeds_handle_t *ppeds_handle, uint8_t intr_enable)
{
	return 0;
}
EXPORT_SYMBOL(edma_ppeds_inst_start);

/*
 * edma_ppeds_inst_stop()
 *	PPE-DS EDMA instance stop API
 */
void edma_ppeds_inst_stop(edma_ppeds_handle_t *ppeds_handle, uint8_t intr_enable)
{
	return;
}
EXPORT_SYMBOL(edma_ppeds_inst_stop);

/*
 * edma_ppeds_inst_del()
 *	PPE-DS EDMA instance delete API
 */
void edma_ppeds_inst_del(edma_ppeds_handle_t *ppeds_handle)
{
	return;
}
EXPORT_SYMBOL(edma_ppeds_inst_del);

/*
 * edma_ppeds_inst_alloc()
 *	PPE-DS EDMA instance allocation API
 */
edma_ppeds_handle_t *edma_ppeds_inst_alloc(const struct edma_ppeds_ops *ops, size_t priv_size)
{
	return NULL;
}
EXPORT_SYMBOL(edma_ppeds_inst_alloc);
#endif

/*
 * edma_ppeds_deinit()
 *	PPEDS deinit
 */
void edma_ppeds_deinit(struct edma_ppeds_drv *drv)
{
	uint32_t i;

	for (i = 0; i < EDMA_PPEDS_MAX_NODES; i++) {
		drv->ppeds_db[i] = NULL;
		drv->node_state[i] = EDMA_PPEDS_NODE_STATE_AVAIL;
	}
}

/*
 * edma_ppeds_init()
 *	PPEDS init
 */
int edma_ppeds_init(struct edma_ppeds_drv *drv)
{
	uint32_t i;

	rwlock_init(&drv->lock);

	for (i = 0; i < EDMA_PPEDS_MAX_NODES; i++) {
		drv->ppeds_db[i] = NULL;
		if (i < drv->num_nodes) {
			drv->node_state[i] = EDMA_PPEDS_NODE_STATE_AVAIL;
			continue;
		}
		drv->node_state[i] = EDMA_PPEDS_NODE_STATE_NOT_AVAIL;
	}

	return 0;
}
